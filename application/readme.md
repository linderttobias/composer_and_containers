PROJECT=experimentplanet
REPO_NAME=testrepo
LOCATION=europe-west1
LABELS="owner=tobias,project=testing"
DESCRIPTION="Repository for Testing"

gcloud artifacts repositories create $REPO_NAME \
    --repository-format=docker \
    --location=$LOCATION \
    --labels=$LABELS \
    --description=$DESCRIPTION \
    --project=$PROJECT

gcloud artifacts repositories describe $REPO_NAME --location $LOCATION


gcloud auth configure-docker europe-west1-docker.pkg.dev

APP=simpleapp

docker tag $APP $LOCATION-docker.pkg.dev/$PROJECT/$REPO_NAME/$APP:latest
docker push $LOCATION-docker.pkg.dev/$PROJECT/$REPO_NAME/$APP:latest