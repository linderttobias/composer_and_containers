# https://click.palletsprojects.com/
import click
from google.cloud import storage


@click.command()
@click.option(
    "--bucket_name",
    help="bucket name",
    prompt=False,
)
def main(bucket_name):
    _list_blobs(bucket_name)


def _list_blobs(bucket_name):
    storage_client = storage.Client()
    blobs = storage_client.list_blobs(bucket_name)
    for blob in blobs:
        print(blob.name)


if __name__ == "__main__":
    main()
